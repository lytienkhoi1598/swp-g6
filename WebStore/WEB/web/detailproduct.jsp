<%-- 
    Document   : detailproduct
    Created on : Feb 28, 2024, 10:59:57 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Product Detail</title>
    </head>
    <body>
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            body{
                margin-top:20px;
                background:#eee;
            }

            /*panel*/
            .panel {
                border: none;
                box-shadow: none;
            }

            .panel-heading {
                border-color:#eff2f7 ;
                font-size: 16px;
                font-weight: 300;
            }

            .panel-title {
                color: #2A3542;
                font-size: 14px;
                font-weight: 400;
                margin-bottom: 0;
                margin-top: 0;
                font-family: 'Open Sans', sans-serif;
            }

            /*product list*/

            .prod-cat li a{
                border-bottom: 1px dashed #d9d9d9;
            }

            .prod-cat li a {
                color: #3b3b3b;
            }

            .prod-cat li ul {
                margin-left: 30px;
            }

            .prod-cat li ul li a{
                border-bottom:none;
            }
            .prod-cat li ul li a:hover,.prod-cat li ul li a:focus, .prod-cat li ul li.active a , .prod-cat li a:hover,.prod-cat li a:focus, .prod-cat li a.active{
                background: none;
                color: #ff7261;
            }

            .pro-lab{
                margin-right: 20px;
                font-weight: normal;
            }

            .pro-sort {
                padding-right: 20px;
                float: left;
            }

            .pro-page-list {
                margin: 5px 0 0 0;
            }

            .product-list img{
                width: 100%;
                border-radius: 4px 4px 0 0;
                -webkit-border-radius: 4px 4px 0 0;
            }

            .product-list .pro-img-box {
                position: relative;
            }
            .adtocart {
                background: #fc5959;
                width: 50px;
                height: 50px;
                border-radius: 50%;
                -webkit-border-radius: 50%;
                color: #fff;
                display: inline-block;
                text-align: center;
                border: 3px solid #fff;
                left: 45%;
                bottom: -25px;
                position: absolute;
            }

            .adtocart i{
                color: #fff;
                font-size: 25px;
                line-height: 42px;
            }

            .pro-title {
                color: #5A5A5A;
                display: inline-block;
                margin-top: 20px;
                font-size: 16px;
            }

            .product-list .price {
                color:#fc5959 ;
                font-size: 15px;
            }

            .pro-img-details {
                margin-left: -15px;
            }

            .pro-img-details img {
                width: 100%;
            }

            .pro-d-title {
                font-size: 16px;
                margin-top: 0;
            }

            .product_meta {
                border-top: 1px solid #eee;
                border-bottom: 1px solid #eee;
                padding: 10px 0;
                margin: 15px 0;
            }

            .product_meta span {
                display: block;
                margin-bottom: 10px;
            }
            .product_meta a, .pro-price{
                color:#fc5959 ;
            }

            .pro-price, .amount-old {
                font-size: 18px;
                padding: 0 10px;
            }

            .amount-old {
                text-decoration: line-through;
            }

            .quantity {
                width: 120px;
            }

            .pro-img-list {
                margin: 10px 0 0 -15px;
                width: 100%;
                display: inline-block;
            }

            .pro-img-list a {
                float: left;
                margin-right: 10px;
                margin-bottom: 10px;
            }

            .pro-d-head {
                font-size: 18px;
                font-weight: 300;
            }

        </style>
    </head>

    <div class="container bootdey">
        <div class="col-xs-6">
            <a href="home" class="btn btn-primary" style="margin-left: -100px" <span>Home</span></a>						
        </div>
        <form action="buy" method="post" name="f">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <c:set var="p" value="${requestScope.product}"/>
                        <c:set var="c" value="${requestScope.category}"/>
                        <div class="col-md-6">
                            <div class="pro-img-details">
                                <img src="${p.image}" alt>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <h4 class="pro-d-title">
                                <a href="#" class>
                                    ${p.name}
                                </a>
                            </h4>
                            <p>
                                ${p.describe}
                            </p>
                            <div class="product_meta">
                                <span class="posted_in"> <strong>Categories:</strong> ${c.name} </span>
                                <span class="tagged_as"><strong>Designer:</strong> Lý Tiến Khôi.</span>
                            </div>
                            <div class="m-bot15"> <strong>Price : </strong> <span class="pro-price"> ${p.price} $</span></div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input type="number" placeholder="1" class="form-control quantity" value="1" name="num" required>
                            </div>
                            <p>
                                <input type="submit" onclick="buy('${p.id}')" value="ADD TO CART" />
                            </p>

                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        function buy(id) {
            var m = document.f.num.value;
            document.f.action = "buy?id=" + id + "&num=" + m;
            document.f.submit();
        }
    </script>
</body>
</html>
