<%-- 
    Document   : home
    Created on : Jan 17, 2024, 11:47:51 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LTK STORE</title>
        <link rel="stylesheet" href="./assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/home.css"/>
    </head>
    <body>
        <%@include file="header.jsp" %>

        <div class="container" style="margin-top: -20px">

            <div>
                <nav class="navbar navbar-expand bg-light justify-content-center" id="navigation">
                    <!-- Links -->
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="home">HOME</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="products">PRODUCT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONTRACT US</a>
                        </li>
                        <li class="nav-item">
                            <a href="show">
                                <img src="img/giohang.png" style="width: 45px;height: 45px;margin-left: 10px;margin-top: 5px" alt/>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="test">
                <h1 style="text-align: center; margin-top: 20px">
                    TOP SẢN PHẨM NỔI BẬT
                </h1>
            </div>

            <c:set var="data" value="${requestScope.data}"/>
            <div class="row">
                <c:forEach items="${data}" var="p">
                    <div class="card col-3 " >
                        <img src="${p.image}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">${p.name}</h5>
                            <p class="card-text">Price: ${p.price} $</p>
                            <a href="detailproduct?id=${p.id}" class="btn btn-primary">Detail</a>


                        </div>
                    </div>
                </c:forEach>
            </div>
            <br/>
            <div class="test">
                <h1 style="text-align: center; margin-top: 20px; margin-bottom: 20px">
                    TOP SẢN PHẨM MỚI RA MẮT
                </h1>
            </div>

            <c:set var="data1" value="${requestScope.data1}"/>
            <div class="row">
                <c:forEach items="${data1}" var="p1">
                    <div class="card col-3 " >
                        <img src="${p1.image}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">${p1.name}</h5>
                            <p class="card-text">Price: ${p1.price} $</p>
                            <a href="detailproduct?id=${p1.id}" class="btn btn-primary">Detail</a>

                        </div>
                    </div>
                </c:forEach>
            </div>
            <br/>
            <div class="test">
                <h1 style="text-align: center; margin-top: 20px; margin-bottom: 20px">
                    BEST SELLER
                </h1>
            </div>

            <c:set var="data2" value="${requestScope.data2}"/>
            <div class="row">
                <c:forEach items="${data2}" var="p2">
                    <div class="card col-3 " >
                        <img src="${p2.image}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">${p2.name}</h5>
                            <p class="card-text">Price: ${p2.price} $</p>
                            <a href="detailproduct?id=${p2.id}" class="btn btn-primary">Detail</a>

                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <br/>
        <%@include file="footer.jsp" %>







        <script>
            function check() {
                let name = document.querySelector('#search').value;
                if (name == null || name.length == 0) {
                    window.alert('not blank!!!');
                }
            }

        </script>
    </body>
</body>
</html>
