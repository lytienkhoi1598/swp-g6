<%-- 
    Document   : create
    Created on : Jan 23, 2024, 1:45:10 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="assets/css/login.css"/>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <title>Create Account</title>
    </head>
    <body>
        <section style="background-color: #eee;">
            <div class="container py-5">
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-xl-10 justify-content-center">
                        <div class="card rounded-3 text-black">
                            <div class="row" style="justify-content: center" >
                                <div >
                                    <div class="card-body" style="width: 350px;height: 900px">

                                        <div class="text-center">
                                            <img src="img/logo.png"
                                                 style="width: 185px;" alt="logo">
                                            <h4 class="mt-1 mb-5 pb-1">LTK's Store</h4>
                                        </div>
                                        <p style="color: red;text-align: center">${requestScope.error12}</p>
                                        <p style="color: red;text-align: center">${requestScope.error1}</p>
                                        <form action="create" method="post" >
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example11">Username</label>
                                                <input type="text" id="form2Example11" class="form-control"
                                                       placeholder="Phone number or address" name="user" required/>

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Password</label>
                                                <input type="password" id="form2Example22" class="form-control" name="password" required />

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Confirm Password</label>
                                                <input type="password" id="form2Example22" class="form-control" name="passwordagain" required />

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Address</label>
                                                <input type="text " id="form2Example22" class="form-control" name="address" required />

                                            </div>
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Phone</label>
                                                <input type="number" id="form2Example22" class="form-control" name="phone" required />

                                            </div>

                                            <div class="text-center pt-1 mb-5 pb-1">
                                                <input class="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3" type="submit" value="Create"/>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
