<%-- 
    Document   : statistical
    Created on : Mar 10, 2024, 10:04:17 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title>Statistical Table</title>
    </head>
    <body  >
        <div style="margin:30px">
            <div class="container">  
                <h1>Statistical Table</h1>  

                <table class="table">  
                    <tr>
                        <th>Name</th>
                        <th>Maximum amount per purchase</th>
                        <th>Minimum amount per purchase</th>
                        <th>Total number of purchases</th>
                        <th>Total amount purchased</th>
                    </tr>  
                    <c:forEach items="${requestScope.data}" var="c">

                        <tr>
                            <td>${c.usernameOrder}</td>
                            <td>
                                ${c.max}
                            </td>
                            <td>
                                ${c.min}
                            </td>
                            <td>
                                ${c.count}
                            </td>
                            <td>
                                ${c.totalPrice}
                            </td>
                        </tr>

                    </c:forEach>
                </table>


            </div>  
        </div>
    </body>
</html>
