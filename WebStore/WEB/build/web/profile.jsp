<%-- 
    Document   : profile
    Created on : Feb 20, 2024, 2:39:11 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="assets/css/login.css"/>
<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profile</title>
    </head>
    <body>
        <section style="background-color: #eee;">
            <div class="container py-5">
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-xl-10 justify-content-center">
                        <div class="card rounded-3 text-black">
                            <div class="row" style="justify-content: center" >
                                <div >
                                    <div class="card-body" style="width: 350px;height: 1100px">

                                        <div class="text-center">
                                            <img src="img/logo.png"
                                                 style="width: 185px;" alt="logo">
                                            <h4 class="mt-1 mb-5 pb-1">LTK's Store</h4>
                                        </div>
                                        <c:set var="acc" value="${sessionScope.acc}"/>
                                        <form >
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example11">Username</label>
                                                <input type="text" id="form2Example11" class="form-control"
                                                       readonly="" name="user" value="${acc.user}"/>

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Password</label>
                                                <input type="password" id="form2Example22" class="form-control" name="pass" value="${acc.pass}"  readonly/>

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Address</label>
                                                <input type="text" id="form2Example22" class="form-control" name="address" 
                                                       value="${acc.address}" readonly />

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Phone</label>
                                                <input type="number" id="form2Example22" class="form-control" name="phone" 
                                                       value="${acc.phone}" readonly />

                                            </div>
                                            <div class="row" style="display: flex;justify-content: space-evenly">
                                                <div>
                                                    <a class="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3" href="updateaccount">Update</a>
                                                </div>

                                                <c:if test="${sessionScope.acc.role == 1}">
                                                    <li style="list-style: none" class="nav-item">
                                                        <a class="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3" onclick="doDeleteA('${acc.user}')" href="#">Delete</a>
                                                    </li>
                                                </c:if>
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>


    <script type="text/javascript">
        function doDeleteA(user) {
            if (confirm('are U sure to delete Username ' + user + ' ?')) {
                //xoa
                window.location = 'deleteA?user=' + user;
            }
        }
    </script>
</html>
