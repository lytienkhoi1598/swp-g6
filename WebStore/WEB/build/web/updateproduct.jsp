<%-- 
    Document   : updateproduct
    Created on : Feb 16, 2024, 9:58:33 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="assets/css/login.css"/>
<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Product</title>
    </head>
    <body>
        <section style="background-color: #eee;">
            <div class="container py-5">
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-xl-10 justify-content-center">
                        <div class="card rounded-3 text-black">
                            <div class="row" style="justify-content: center" >
                                <div >
                                    <div class="card-body" style="width: 350px;height: 1100px">

                                        <div class="text-center">
                                            <img src="img/logo.png"
                                                 style="width: 185px;" alt="logo">
                                            <h4 class="mt-1 mb-5 pb-1">LTK's Store</h4>
                                        </div>
                                         <c:set var="c" value="${requestScope.product}"/>
                                        <form action="updateproduct" method="post" >
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example11">ID</label>
                                                <input type="text" id="form2Example11" class="form-control"
                                                       readonly name="id" value="${c.id}"/>

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Name Products</label>
                                                <input type="text" id="form2Example22" class="form-control" name="name" value="${c.name}" required />

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Quantity</label>
                                                <input type="number" id="form2Example22" class="form-control" name="quantity" 
                                                       value="${c.quantity}" required />

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Price</label>
                                                <input type="number" id="form2Example22" class="form-control" name="price" 
                                                        value="${c.price}" required />

                                            </div>
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Release Date</label>
                                                <input type="date" id="form2Example22" class="form-control" name="releasedate" 
                                                        value="${c.releaseDate}" required />

                                            </div>
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Describe</label>
                                                <input type="text" id="form2Example22" class="form-control" name="describe"
                                                       value="${c.describe}" required />

                                            </div>
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Img</label>
                                                <input type="file" id="form2Example22" class="form-control" name="image"
                                                        value="${c.image}" required />

                                            </div>
                                            <div>
                                                Category: <select name="cid" >
                                                    <c:forEach items="${requestScope.data1}" var="c">
                                                        <option ${requestScope.cid eq c.id?'selected':''} value="${c.id}">${c.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>


                                            <div class="text-center pt-1 mb-5 pb-1">
                                                <input class="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3" type="submit" value="Update"/>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
