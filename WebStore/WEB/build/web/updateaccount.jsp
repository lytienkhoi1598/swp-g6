<%-- 
    Document   : updateaccount
    Created on : Feb 20, 2024, 2:41:19 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="assets/css/login.css"/>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <title>Update Account</title>
    </head>
    <body>
        <section style="background-color: #eee;">
            <div class="container py-5">
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-xl-10 justify-content-center">
                        <div class="card rounded-3 text-black">
                            <div class="row" style="justify-content: center" >
                                <div >
                                    <div class="card-body" style="width: 350px;height: 1100px">

                                        <div class="text-center">
                                            <img src="img/logo.png"
                                                 style="width: 185px;" alt="logo">
                                            <h4 class="mt-1 mb-5 pb-1">LTK's Store</h4>
                                        </div>
                                        <h5 style="color: red;margin-left: -40px; width: 500px">${requestScope.mess}</h5>
                                        <c:set var="acc" value="${sessionScope.acc}"/>
                                        <form action="updateaccount" method="post" >
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example11">Username</label>
                                                <input type="text" id="form2Example11" class="form-control"
                                                       name="user" value="${acc.user}" readonly/>

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Old Password</label>
                                                <input type="text" id="form2Example22" class="form-control" name="oldpass" value="${acc.pass}" readonly/>

                                            </div>
                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">New Password</label>
                                                <input type="password" id="form2Example22" class="form-control" name="newpass" required/>

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Confirm Password</label>
                                                <input type="password" id="form2Example22" class="form-control" name="confirmpass" required />

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Address</label>
                                                <input type="text" id="form2Example22" class="form-control" name="address" 
                                                       value="${acc.address}" required />

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Phone</label>
                                                <input type="number" id="form2Example22" class="form-control" name="phone" 
                                                       value="${acc.phone}" required />

                                            </div>
                                            <div class="text-center pt-1 mb-5 pb-1">
                                                <input class="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3" type="submit" value="Update"/>
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
