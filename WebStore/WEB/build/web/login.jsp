<%-- 
    Document   : login
    Created on : Jan 17, 2024, 10:16:56 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="assets/css/login.css"/>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <title>Login</title>
    </head>
    <body>
        <section class="h-100 gradient-form " style="background-color: #eee;">
            <div class="container py-5 h-100 ">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-xl-10 justify-content-center">
                        <div class="card rounded-3 text-black">
                            <div class="row" style="justify-content: center" >
                                <div >
                                    <div class="card-body" style="width: 350px;height: 800px">

                                        <div class="text-center">
                                            <img src="img/logo.png"
                                                 style="width: 185px;" alt="logo">
                                            <h4 class="mt-1 mb-5 pb-1">LTK's Store</h4>
                                        </div>
                                        <div style="color: red;text-align: center">${requestScope.mess}</div>
                                        <c:set var="cookie" value="${pageContext.request.cookies}"/>
                                        <form action="login" method="post">

                                            <div class="form-outline mb-4">
                                                <label class="form-label"  for="form2Example11">Username</label>
                                                <input type="text" id="form2Example11" class="form-control"
                                                       placeholder="Phone number or email address" name="user" value="${cookie.cname.value}" required/>

                                            </div>

                                            <div class="form-outline mb-4">
                                                <label class="form-label" for="form2Example22">Password</label>
                                                <input type="password" id="form2Example22" class="form-control" name="password" value="${cookie.cpass.value}" required/>

                                            </div>


                                            <div style="margin-top: 0px">
                                                <input ${(cookie.crem!=null?'checked':'')} type="checkbox"name="remember" value="ON" />Remember me
                                            </div>

                                            <a href="https://accounts.google.com/o/oauth2/auth?scope=email%20profile%20openid&redirect_uri=http://localhost:8080/WEB/LoginGoogleHandler&response_type=code&client_id=406240029679-tser79dm9n4l3ci224sndgabgo8u5lun.apps.googleusercontent.com&approval_prompt=force">Login With Google</a>




                                            <div class="text-center pt-1 mb-5 pb-1">
                                                <button class="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3" type="submit">
                                                    Login
                                                </button>                                               
                                            </div>
                                        </form>

                                        <div class="d-flex align-items-center justify-content-center pb-4">
                                            <p class="mb-0 me-2">Don't have an account?</p>

                                            <a href="create"   class="btn btn-outline-danger"/>Create new</a>

                                        </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
