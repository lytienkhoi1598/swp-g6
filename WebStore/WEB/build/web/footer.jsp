<%-- 
    Document   : footer
    Created on : Jan 26, 2024, 1:16:18 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/home.css"/>
    </head>
    <body>
        <div class="row" id="footer-content" style="justify-content: space-evenly">
            <div class="col-md-3">
                <h3>ĐỊA CHỈ MUA HÀNG</h3>
                <h5>ADDRESS: Di Trạch, Hoài Đức, Hà Nội</h5>
                <h5>HOTLINE: 0962698931</h5>
            </div>  
            <div class="col-md-3">
                <h3>HỖ TRỢ MUA HÀNG</h3>
                <a href="url">Chính sách đổi trả</a>     
                <br/>
                <a href="url">Chính sách bảo hành</a>
            </div>  
            <div class="col-md-3" >
                <h3>KẾT NỐI</h3>
                <h5>
                    <a href="">
                        <img src="img/face.png" style="width: 30px; height: 30px"/>
                    </a>
                    <a href="">
                        <img src="img/png-clipart-youtube-computer-icons-youtube-angle-rectangle-thumbnail.png" style="width: 30px; height: 30px" />
                    </a>
                    <a href="">
                        <img src="img/tw.png" style="width: 30px; height: 30px"/>
                    </a>
                </h5>
                <h5>GMAIL: LTKStore@gmail.com</h5>
            </div>  


        </div>
    </body>
</html>
