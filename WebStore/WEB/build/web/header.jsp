<%-- 
    Document   : header
    Created on : Jan 26, 2024, 1:15:21 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Header</title>
        <link rel="stylesheet" href="./assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/home.css"/>
    </head>
    <body>
        <div class="row" style="justify-content: space-evenly;padding-bottom: 25px" >
            <div id="logo"> <img style="width: 55%" src="img/logo.png" alt=""/></div>
            <div style="justify-content: end">  
                <div class="row justify-content-end" >
                    <ul class="row" style="list-style: none">
                        <c:if test="${sessionScope.acc.role == 0}">
                            <li class="nav-item">
                                <a class="nav-link" href="statistical">Statistical</a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.acc.role == 0}">
                            <li class="nav-item">
                                <a class="nav-link" href="crudcategory">Manager Categories</a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.acc.role == 0}">
                            <li class="nav-item">
                                <a class="nav-link" href="crud">Manager Product</a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.acc.role == 0}">
                            <li class="nav-item">
                                <a class="nav-link" href="crudaccount">Manager Account</a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.acc != null}">
                            <li class="nav-item">
                                <a class="nav-link" href="changepass">Change Password</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="profile">Hello${sessionScope.acc.user}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="logout">Logout</a>
                            </li> 
                        </c:if>
                        <c:if test="${sessionScope.acc == null}">
                            <li class="nav-item"">
                                <a class="nav-link" href="login">Log in/Sign up </a>
                            </li>
                        </c:if>
                    </ul>

                </div>
                <div>
                    <form class="form-inline" style="margin-left: 100px;margin-top: 150px" action="products">
                        <input name="key" id="search" class="form-control mr-sm-2" type="search" style="width: 300px" placeholder="Search" aria-label="Search">
                        <button onclick="check()" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>

            </div>       

        </div>

        <script>
            function check() {
                let name = document.querySelector('#search').value;
                if (name == null || name.length == 0) {
                    window.alert('not blank!!!');
                }
            }

        </script>
    </body>
</html>
