/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.time.LocalDateTime;

/**
 *
 * @author admin
 */
public class Order {

    private int orderID;

    private String usernameOrder;
    private Date orderDate;
    private double totalPrice;
    private int count;
    private double max;
    private double min;

    public Order(String usernameOrder, double max, double min, int count, double totalPrice) {
        this.usernameOrder = usernameOrder;
        this.totalPrice = totalPrice;
        this.count = count;
        this.max = max;
        this.min = min;
    }

    public Order() {
    }

    public Order(String usernameOrder, double totalPrice) {
        this.usernameOrder = usernameOrder;
        this.totalPrice = totalPrice;
    }

    public Order(int orderID, String usernameOrder, Date orderDate, double totalPrice) {
        this.orderID = orderID;
        this.usernameOrder = usernameOrder;
        this.orderDate = orderDate;
        this.totalPrice = totalPrice;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getUsernameOrder() {
        return usernameOrder;
    }

    public void setUsernameOrder(String usernameOrder) {
        this.usernameOrder = usernameOrder;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
