/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import com.sun.jdi.connect.spi.Connection;
import java.security.interfaces.RSAKey;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collections;
import model.Cart;
import model.Category;
import model.Item;
import model.Order;
import model.Product;

public class DAO extends DBContext {

    //doc tat ca cac ban ghi
    public List<Account> getAccount() {
        List<Account> list = new ArrayList<>();
        String sql = "select * from Accounts";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account c = new Account(rs.getString("username"), rs.getString("pass"), rs.getString("address"), rs.getString("phone"), rs.getInt("role"));
                list.add(c);

            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Category> getCategoryAll() {
        List<Category> list = new ArrayList<>();
        String sql = "SELECT *  FROM Categories";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = new Category();
                c.setId(rs.getInt("categoryID"));
                c.setName(rs.getString("categoryName"));
                list.add(c);

            }
        } catch (SQLException e) {

        }
        return list;
    }

    //kiem tra id da co chua?
    public Category getCategoryById(int id) {
        String sql = "SELECT [categoryID]\n"
                + "      ,[categoryName]\n"
                + "  FROM [dbo].[Categories] where categoryID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Category c = new Category();
                c.setId(rs.getInt("categoryID"));
                c.setName(rs.getString("categoryName"));
                return c;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public Category getCategoryByProductID(int id) {
        String sql = "    SELECT [CategoryID]\n"
                + "      ,[CategoryName]\n"
                + "  FROM [dbo].[Categories]\n"
                + "  join Products on Categories.CategoryID = Products.cid\n"
                + "	where ProductID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Category c = new Category();
                c.setId(rs.getInt("categoryID"));
                c.setName(rs.getString("categoryName"));
                return c;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public void insertAccount(Account c) {
        String sql = "INSERT INTO [dbo].[Accounts]\n"
                + "           ([Username]\n"
                + "           ,[Pass]\n"
                + "           ,[Address]\n"
                + "           ,[Phone]\n"
                + "           ,[Role])"
                + "     VALUES\n"
                + "           (?,?,?,?,1)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getUser());
            st.setString(2, c.getPass());
            st.setString(3, c.getAddress());
            st.setString(4, c.getPhone());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public Account login(String user, String pass) {
        String sql = "select * from Accounts\n"
                + "where username = ?\n"
                + "and Pass = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getString("username"),
                        rs.getString("pass"),
                        rs.getString("address"),
                        rs.getString("phone"),
                        rs.getInt("role"));
            }
        } catch (Exception e) {
        }
        return null;
    }

//    public Account getAccountsbyUser(String user) {
//        String sql = "select * from Accounts where username='" + user + "'";
//        try {
//            PreparedStatement st = connection.prepareStatement(sql);
//            ResultSet rs = st.executeQuery();
//            if (rs.next()) {
//                Account c = new Account(rs.getString("username"), rs.getString("pass"), rs.getString("address"), rs.getInt("phone"), rs.getInt("role"));
//                return c;
//            }
//
//        } catch (SQLException e) {
//            System.out.println(e);
//        }
//        return null;
//    }
//    //delete a category
//    public void delete(int id) {
//        String sql = "DELETE FROM [dbo].[Categories]\n"
//                + "      WHERE ID=?";
//
//        try {
//            PreparedStatement st = connection.prepareStatement(sql);
//            st.setInt(1, id);
//            st.executeUpdate();
//        } catch (SQLException e) {
//            System.out.println(e);
//        }
//
//    }
//
//    public void update(Category c) {
//        String sql = "UPDATE [dbo].[Categories]\n"
//                + "   SET [name] = ?, [describe] = ?\n"
//                + " WHERE ID = ?";
//
//        try {
//            PreparedStatement st = connection.prepareStatement(sql);
//
//            st.setString(1, c.getName());
//            st.setString(2, c.getDescribe());
//            st.setInt(3, c.getId());
//            st.executeUpdate();
//
//        } catch (SQLException e) {
//            System.out.println(e);
//        }
//    }
    public List<Product> getProductsByKey(String key) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT p.ProductID\n"
                + "      ,p.ProductName\n"
                + "      ,p.quantity\n"
                + "      ,p.price\n"
                + "      ,p.releaseDate\n"
                + "      ,p.describe\n"
                + "      ,p.Image\n"
                + "      ,c.categoryid as cid, c.CategoryName as cname\n"
                + "  FROM Products p inner join categories c on(c.categoryid=p.cid) "
                + "where p.ProductName like N'%" + key + "%'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"),
                        new Category(rs.getInt("cid"),
                                rs.getString("cname")));
                list.add(p);
            }
        } catch (SQLException e) {

        }

        return list;
    }

    public List<Product> getProductsTopOutstanding(String key) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT top 8 p.ProductID\n"
                + "      ,p.ProductName\n"
                + "      ,p.quantity\n"
                + "      ,p.price\n"
                + "      ,p.releaseDate\n"
                + "      ,p.describe\n"
                + "      ,p.Image\n"
                + "      ,c.categoryid as cid, c.CategoryName as cname\n"
                + "  FROM Products p inner join categories c on(c.categoryid=p.cid) "
                + "where p.ProductName like N'%" + key + "%'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"),
                        new Category(rs.getInt("cid"),
                                rs.getString("cname")));
                list.add(p);
            }
        } catch (SQLException e) {

        }

        return list;
    }

    public List<Product> getProductsTopNewLaunched(String key) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT top 8 p.ProductID\n"
                + "      ,p.ProductName\n"
                + "      ,p.quantity\n"
                + "      ,p.price\n"
                + "      ,p.releaseDate\n"
                + "      ,p.describe\n"
                + "      ,p.Image\n"
                + "      ,c.categoryid as cid, c.CategoryName as cname\n"
                + "  FROM Products p inner join categories c on(c.categoryid=p.cid) "
                + "where p.ProductName like N'%" + key + "%' ORDER BY releaseDate DESC ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"),
                        new Category(rs.getInt("cid"),
                                rs.getString("cname")));
                list.add(p);
            }
        } catch (SQLException e) {

        }

        return list;
    }

    public List<Product> getProductsBestSeller() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT top 8 Products.ProductID, ProductName, Products.Quantity, Products.Price, \n"
                + "       Products.releaseDate, Products.Describe, Products.Image, Products.cid,\n"
                + "       SUM([Order Details].Quantity) AS totalQuantitytoBuy\n"
                + "FROM Products\n"
                + "JOIN [Order Details] ON Products.ProductID = [Order Details].ProductID\n"
                + "GROUP BY Products.ProductID, ProductName, Products.Quantity, Products.Price, \n"
                + "         Products.releaseDate, Products.Describe, Products.Image, Products.cid\n"
                + "ORDER BY totalQuantitytoBuy DESC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = getCategoryById(rs.getInt("cid"));
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"), c);
                list.add(p);

            }
        } catch (SQLException e) {

        }
        return list;
    }

    public List<Product> getProductsAll() {
        List<Product> list = new ArrayList<>();
        String sql = "select * from products";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = getCategoryById(rs.getInt("cid"));
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"),
                        c);
                list.add(p);
            }
        } catch (SQLException e) {

        }
        return list;
    }

    public List<Product> getProductsByCid(int cid) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT p.ProductID\n"
                + "      ,p.ProductName\n"
                + "      ,p.quantity\n"
                + "      ,p.price\n"
                + "      ,p.releaseDate\n"
                + "      ,p.describe\n"
                + "      ,p.image\n"
                + "      ,c.categoryid as cid, c.CategoryName as cname\n"
                + "  FROM Products p inner join Categories c on(c.categoryid=p.cid)"
                + " where 1=1";

        if (cid != 0) {
            sql += " and cid=" + cid;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
//            st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"),
                        new Category(rs.getInt("cid"),
                                rs.getString("cname")));
                list.add(p);
            }
        } catch (SQLException e) {

        }

        return list;
    }

    public List<Product> getProductsByCid1(int cid) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT p.ProductID\n"
                + "      ,p.ProductName\n"
                + "      ,p.quantity\n"
                + "      ,p.price\n"
                + "      ,p.releaseDate\n"
                + "      ,p.describe\n"
                + "      ,p.image\n"
                + "  FROM Products where 1=1";

        if (cid != 0) {
            sql += " and cid=" + cid;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
//            st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("ProductID"));
                p.setName(rs.getString("ProductName"));
                p.setQuantity(rs.getInt("Quantity"));
                p.setPrice(rs.getInt("Price"));
                p.setReleaseDate(rs.getDate("releaseDate"));
                p.setDescribe(rs.getString("Describe"));
                p.setImage(rs.getString("Image"));
                Category c = getCategoryById(rs.getInt("cid"));
                p.setCid(c);
                list.add(p);

            }
        } catch (SQLException e) {

        }

        return list;
    }

    public Product getProductById(int id) {
        String sql = "SELECT [ProductID]\n"
                + "      ,[ProductName]\n"
                + "      ,[Quantity]\n"
                + "      ,[Price]\n"
                + "      ,[releaseDate]\n"
                + "      ,[Describe]\n"
                + "      ,[Image],\n"
                + "  c.categoryid as cid, c.CategoryName as cname\n"
                + "  FROM Products p inner join Categories c on(c.categoryid=p.cid)\n"
                + "  where ProductID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product c = new Product();
                c.setId(rs.getInt("ProductID"));
                c.setName(rs.getString("ProductName"));
                c.setQuantity(rs.getInt("Quantity"));
                c.setPrice(rs.getInt("Price"));
                c.setReleaseDate(rs.getDate("releaseDate"));
                c.setDescribe(rs.getString("Describe"));
                c.setImage(rs.getString("Image"));
                c.setCid(new Category(rs.getInt("cid"), rs.getString("cname")));
                return c;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public Product getProductsById1(int id) {
        String sql = "select * from products where ProductID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Category c = getCategoryById(rs.getInt("cid"));
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("Quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"), c);
                return p;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public void deleteProduct(int id) {
        String sql = "DELETE FROM [dbo].[Products]\n"
                + "      WHERE ProductID = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteCategory(int id) {
        String sql = "DELETE FROM [dbo].[Categories]\n"
                + "      WHERE CategoryID = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteAccount(String user) {
        String sql = "DELETE FROM Accounts\n"
                + "      WHERE Username = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateProduct(Product p) {
        String sql = "UPDATE [dbo].[Products]\n"
                + "   SET [ProductName] = ?\n"
                + "      ,[Quantity] = ?\n"
                + "      ,[Price] = ?\n"
                + "      ,[releaseDate] = ?\n"
                + "      ,[Describe] = ?\n"
                + "      ,[Image] = ? \n"
                + "      ,[cid] = ?\n"
                + " WHERE ProductID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, p.getName());
            st.setInt(2, p.getQuantity());
            st.setDouble(3, p.getPrice());
            st.setDate(4, p.getReleaseDate());
            st.setString(5, p.getDescribe());
            st.setString(6, p.getImage());
            st.setInt(7, p.getCid().getId());
            st.setInt(8, p.getId());
            st.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public void updateCategory(Category c) {
        String sql = "UPDATE [dbo].[Categories]\n"
                + "   SET [CategoryName] = ?\n"
                + " WHERE CategoryID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getName());
            st.setInt(2, c.getId());
            st.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public void insertProduct(Product p) {
        String sql = "insert into Products values(?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, p.getId());
            st.setString(2, p.getName());
            st.setInt(3, p.getQuantity());
            st.setDouble(4, p.getPrice());
            st.setDate(5, p.getReleaseDate());
            st.setString(6, p.getDescribe());
            st.setString(7, p.getImage());
            st.setInt(8, p.getCid().getId());
            st.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public void insertCategory(Category c) {
        String sql = "insert into Categories values(?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, c.getId());
            st.setString(2, c.getName());
            st.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public List<Product> searchByPrice(int price) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT p.ProductID\n"
                + "      ,p.ProductName\n"
                + "      ,p.quantity\n"
                + "      ,p.price\n"
                + "      ,p.releaseDate\n"
                + "      ,p.describe\n"
                + "      ,p.image\n"
                + "      ,c.categoryid as cid, c.CategoryName as cname\n"
                + "  FROM Products p inner join categories c on(c.categoryid=p.cid)"
                + "where 1=1";

        if (price == 0) {
            sql += "order by price desc";
        }
        if (price == 1) {
            sql += "order by price asc";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"),
                        new Category(rs.getInt("cid"),
                                rs.getString("cname")));
                list.add(p);
            }
        } catch (SQLException e) {

        }

        return list;
    }

    public List<Product> search(int cid, String key) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT p.ProductID\n"
                + "      ,p.ProductName\n"
                + "      ,p.quantity\n"
                + "      ,p.price\n"
                + "      ,p.releaseDate\n"
                + "      ,p.describe\n"
                + "      ,p.image\n"
                + "      ,c.categoryid as cid, c.CategoryName as cname\n"
                + "  FROM Products p inner join categories c on(c.categoryid=p.cid)"
                + " where cid=? and p.ProductName like N'%" + key + "%'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getInt("ProductID"),
                        rs.getString("ProductName"),
                        rs.getInt("quantity"),
                        rs.getDouble("price"),
                        rs.getDate("releaseDate"),
                        rs.getString("describe"),
                        rs.getString("image"),
                        new Category(rs.getInt("cid"),
                                rs.getString("cname")));
                list.add(p);
            }
        } catch (SQLException e) {

        }

        return list;
    }

    public boolean checkAccount(String user, String pass) {
        String sql = "select * from Accounts where username=? and pass = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }

            connection.close();

        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public Account getAccountsByUser(String user) {
        String sql = "select * from Accounts where Username=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Account c = new Account(rs.getString("Username"),
                        rs.getString("Pass"),
                        rs.getString("Address"),
                        rs.getString("Phone"),
                        rs.getInt("role"));
                return c;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public void updateAccount(Account p) {
        String sql = "UPDATE [dbo].[Accounts]\n"
                + "   SET [Pass] = ?\n"
                + "      ,[Address] = ?\n"
                + "      ,[Phone] = ?\n"
                + " WHERE Username = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, p.getPass());
            st.setString(2, p.getAddress());
            st.setString(3, p.getPhone());
            st.setString(4, p.getUser());
            st.executeUpdate();
        } catch (SQLException e) {

        }

    }
    
        public void ChangePass(Account p) {
        String sql = "UPDATE [dbo].[Accounts]\n"
                + "   SET [Pass] = ?\n"
                + " WHERE Username = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, p.getPass());
            st.setString(2, p.getUser());
            st.executeUpdate();
        } catch (SQLException e) {

        }

    }

    public void addOrder(Account a, Cart cart) {
        LocalDate curDate = LocalDate.now();
        String date = curDate.toString();
        try {
            String sql = "insert into Orders values(?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getUser());
            st.setString(2, date);
            st.setDouble(3, cart.getTotalMoney());
            st.executeUpdate();
            String sql1 = "select top 1 OrderID from Orders order by OrderID desc";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs = st1.executeQuery();
            if (rs.next()) {
                int oid = rs.getInt("OrderID");
                for (Item i : cart.getItems()) {
                    String sql2 = "insert into [Order Details] values (?,?,?,?)";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setInt(1, oid);
                    st2.setInt(2, i.getProduct().getId());
                    st2.setInt(3, i.getQuantity());
                    st2.setDouble(4, i.getPrice());
                    st2.executeUpdate();

                }

            }
            String sql3 = "update Products set quantity = quantity - ? where ProductID = ?";
            PreparedStatement st3 = connection.prepareStatement(sql3);
            for (Item i : cart.getItems()) {
                st3.setInt(1, i.getQuantity());
                st3.setInt(2, i.getProduct().getId());
                st3.executeUpdate();
            }
        } catch (SQLException e) {

        }
    }

    public List<Order> getStatiscal() {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT  [UsernameOrder],\n"
                + "		max(totalmoney) as maxbuy,\n"
                + "		min(totalmoney) as minbuy,\n"
                + "		count(totalmoney) as totalnumberofpurchases,\n"
                + "		sum(totalmoney)as totalamountpurchased\n"
                + "	 FROM [dbo].[Orders]\n"
                + "	group by UsernameOrder order by totalamountpurchased desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order c = new Order(rs.getString("UsernameOrder"), rs.getDouble("maxbuy"), rs.getDouble("minbuy"), rs.getInt("totalnumberofpurchases"), rs.getDouble("totalamountpurchased"));
                list.add(c);

            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Order> getTotalNumberOfPurchased() {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT UsernameOrder\n"
                + "	  ,count(totalmoney) as totalnumberofpurchases\n"
                + "  FROM [dbo].[Orders]\n"
                + "  group by UsernameOrder";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order c = new Order(rs.getString("UsernameOrder"), rs.getDouble("totalnumberofpurchases"));
                list.add(c);

            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> searchByCheck(int[] cid) {
        List<Product> list = new ArrayList<>();
//        String sql = "select * from products where 1=1";
//        if (cid != null &&cid.length > 0 && cid[0] != 0) {
//            sql += "and cid in(";
//            for (int i = 0; i < cid.length; i++) {
//                sql += cid[i] + ",";
//            }
//            if (sql.endsWith(",")) {
//                sql = sql.substring(0, sql.length() - 1);
//            }
//            sql += ")";
//        }
//        try {
//            PreparedStatement st = connection.prepareStatement(sql);
//            ResultSet rs = st.executeQuery();
//            if (rs.next()) {
//                Product p = new Product();
//                p.setId(rs.getInt("ProductID"));
//                p.setName(rs.getString("ProductName"));
//                p.setQuantity(rs.getInt("Quantity"));
//                p.setPrice(rs.getInt("Price"));
//                p.setReleaseDate(rs.getDate("releaseDate"));
//                p.setDescribe(rs.getString("Describe"));
//                p.setImage(rs.getString("Image"));
//                Category c = getCategoryById(rs.getInt("cid"));
//                p.setCid(c);
//                list.add(p);
//
//            }
//        } catch (SQLException e) {
//            System.out.println(e);
//        }
//        return list;

        String sql = "select * from products where 1=1";
        if (cid[0] == 0) {
            sql += "";
        }
        if (cid != null && cid.length > 0 && cid[0] != 0) {
            sql += " and cid in (";
            // Tạo một chuỗi gồm các dấu "?" phân tách bởi dấu phẩy
            String placeholders = String.join(",", Collections.nCopies(cid.length, "?"));
            sql += placeholders;
            sql += ")";
        }

        try (PreparedStatement st = connection.prepareStatement(sql)) {
            // Đặt giá trị cho mỗi placeholder
            for (int i = 0; i < cid.length; i++) {
                st.setInt(i + 1, cid[i]); // JDBC index bắt đầu từ 1
            }
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    Product p = new Product();
                    p.setId(rs.getInt("ProductID"));
                    p.setName(rs.getString("ProductName"));
                    p.setQuantity(rs.getInt("Quantity"));
                    p.setPrice(rs.getDouble("Price")); // Giả sử giá là double
                    p.setReleaseDate(rs.getDate("ReleaseDate"));
                    p.setDescribe(rs.getString("Describe"));
                    p.setImage(rs.getString("Image"));
                    Category c = getCategoryById(rs.getInt("cid"));
                    p.setCid(c);
                    list.add(p);
                }
                return list;
            }
        } catch (SQLException e) {
            System.out.println(e);
            return Collections.emptyList();
        }
    }

    public List<Product> getProductByPrice(double from, double to) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from products where price between ? and ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDouble(1, from);
            st.setDouble(2, to);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("ProductID"));
                p.setName(rs.getString("ProductName"));
                p.setQuantity(rs.getInt("Quantity"));
                p.setPrice(rs.getInt("Price"));
                p.setReleaseDate(rs.getDate("releaseDate"));
                p.setDescribe(rs.getString("Describe"));
                p.setImage(rs.getString("Image"));
                Category c = getCategoryById(rs.getInt("cid"));
                p.setCid(c);
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> searchByKey(String key) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT [ProductID]\n"
                + "      ,[ProductName]\n"
                + "      ,[Quantity]\n"
                + "      ,[Price]\n"
                + "      ,[releaseDate]\n"
                + "      ,[Describe]\n"
                + "      ,[Image]\n"
                + "      ,[cid]\n"
                + "FROM [dbo].[Products]\n"
                + "WHERE ProductName LIKE ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + key + "%");
            st.setString(2, "%" + key + "%");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("ProductID"));
                p.setName(rs.getString("ProductName"));
                p.setQuantity(rs.getInt("Quantity"));
                p.setPrice(rs.getInt("Price"));
                p.setReleaseDate(rs.getDate("releaseDate"));
                p.setDescribe(rs.getString("Describe"));
                p.setImage(rs.getString("Image"));
                Category c = getCategoryById(rs.getInt("cid"));
                p.setCid(c);
                list.add(p);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        DAO d = new DAO();
//////        List<CreateUser> list = c.getAll();
//////        System.out.println(list.get(0).getName());
//////        System.out.println(d.getStatiscal());
//        int[] categoryIds = new int[]{1, 2, 3};
//        System.out.println(categoryIds.length);
//        // Gọi phương thức searchByCheck với mảng đã tạo
//        List<Product> products = d.searchByCheck(categoryIds);
//
//        // In kết quả
//        for (Product product : products) {
//            System.out.println(product.getName()); // Đảm bảo rằng lớp Product đã ghi đè phương thức toString
//        }

        System.out.println(d.getProductsBestSeller().get(0).getName());
    }

}
