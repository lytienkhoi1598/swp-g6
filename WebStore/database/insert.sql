﻿create table Accounts
(
Username nvarchar(30) primary key,
Pass nvarchar(20) not null,
Address nvarchar(40) not null,
Phone varchar(10) not null,
Role int not null,
)

insert into Accounts values('Admin','123456','Hanoi','0962698931',0);
insert into Accounts values('Manh','111111','Hanoi','0915162845',1);
insert into Accounts values('Huong','222222','HaTay','0378603138',1);
insert into Accounts values('Nam','333333','BacGiang','0123456789',1);
insert into Accounts values('Linh','444444','DucGiang','0987654321',1);
insert into Accounts values('Khanh','555555','LangSon','0147258369',1);
insert into Accounts values('Minh','666666','HaiPhong','0321654987',1);
insert into Accounts values('Dung','777777','GiaLam','0369852147',1);
insert into Accounts values('Bach','888888','SonTay','0248631790',1);

create table Categories
(
CategoryID [int] primary key,
CategoryName nvarchar(20) ,
)

insert into Categories values(1,'Áo thun');
insert into Categories values(2,'Áo polo');
insert into Categories values(3,'Áo khoác');
insert into Categories values(4,'Áo len');

create table Products
(
ProductID int primary key,
ProductName [nvarchar](max) NULL,
Quantity [int] NULL,
Price [money] NULL,
[releaseDate] [date] NULL,
Describe [nvarchar](max) NULL,
Image [nvarchar](max) NULL,
[cid] [int] references Categories(CategoryID),
)

insert into Products values('1',N'Áo Polo Nam Cotton ',50,99.000,'2024-1-1',N'thoáng mát,
không nhăn, polo trơn',N'img/ao1den.jpg',2);

insert into Products values('2',N'Áo thun form Oversize',50,129.000,'2024-1-1',N'Rộng rãi thoải mái che khuyết điểm tốt và dễ dàng hoạt động',N'img/ao2den.jpg',1);

insert into Products values('3',N'Áo thun nam có túi',50,139.000,'2024-1-1',N'Hoàn thiện: tỉ mỉ cao',N'img/ao3den.jpg',1);

insert into Products values('4',N'Áo Polo unisex form Oversize',50,149.000,'2024-1-1',N'Dệt theo nốt jersey và bằng sợi cotton',N'img/ao4den.jpg',2);

insert into Products values('5',N'Áo thun form Oversize phông chữ ',50,189.000,'2024-1-1',N'Cotton thoáng mát, có phông chữ',N'img/ao5den.jpg',1);

insert into Products values('6',N'Áo polo phối vai cá tính',50,199.000,'2024-1-1',N'Cotton thoáng mát,
không nhăn, polo trơn',N'img/ao6den.jpg',2);

insert into Products values('7',N'Áo thun nam',50,249.000,'2024-1-1',N'Vải Cotton thoáng mát, thấm hút, phông trơn',N'img/ao7den.jpg',1);

insert into Products values('8',N'Áo Polo nam Premium trẻ trung',50,299.000,'2024-1-1',N'Cotton thoáng mát,
Polo len kẻ ngang',N'img/ao8den.jpg',2);

insert into Products values('9',N'Áo khoác dạ',50,399.0000,'2024-01-20',N'Dạ ép cao cấp,form suông',N'img/aokhoac9den.jpg',3);

insert into Products values('10',N'Áo khoác dạ tweed',50,429.0000,'2024-01-01',N'Dạ tweed nhập khẩu, vải dày dặn',N'img/aokhoac10.jpg',3);

insert into Products values('11',N'Áo khoác măng tô',50,599.0000,'2024-02-01',N'Dạ ép cao cấp,hoàn thiện tỉ mỉ cao',N'img/aokhoac11.jpg',3);

insert into Products values('12',N'Áo len có khóa',50,319.0000,'2023-12-01',N'Len cao cấp,form suông che khuyết điểm tốt',N'img/aolen12.jpg',4);

insert into Products values('13',N'Áo len kẻ ngang',50,329.0000,'2023-12-10',N'Len cao cấp, dễ phối đồ',N'img/aolen13.jpg',4);

insert into Products values('14',N'Áo len rách',50,339.0000,'2023-12-15',N'Len cao cấp, form áo suông',N'img/aolen14.jpg',4);

insert into Products values('15',N'Áo polo phối ngang',50,199.0000,'2023-08-01',N'Cotton thoáng mát, thấm hút, không nhăn',N'img/aopolo15.jpg',2);

insert into Products values('16',N'Áo khoác chất da PU',50,449.0000,'2024-01-10',N'Da PU nhập khẩu,khóa đồng kéo trơn tru',N'img/aokhoac16.jpg',3);

insert into Products values('17',N'Áo khoác visco',50,449.0000,'2024-01-15',N'Len Visco,sọc caro,form suông',N'img/aokhoac17.jpg',3);

insert into Products values('18',N'Áo khoác lông cừu',50,459.0000,'2024-02-10',N'Áo lông cừu, form unisex,vải dày dặn',N'img/aokhoac18.jpg',3);

insert into Products values('19',N'Áo len phối ngang',50,349.0000,'2023-12-20',N'Len lông cừu,form suông che khuyết điểm tốt',N'img/aolen19.jpg',4);

insert into Products values('20',N'Áo thun phông thêu',50,199.0000,'2023-07-10',N'Chất Cotton thoáng mát,thấm hút,không nhăn',N'img/aothun20.jpg',1);


CREATE TABLE [Orders](
		[OrderID] int identity(1,1) primary key not null
	, [UsernameOrder] nvarchar(30) foreign key references [Accounts]([Username]) not null
	, [OrderDate] date not null
	, totalmoney money null

);	

CREATE TABLE [Order Details](
	[OrderDetailID] int foreign key references [Orders]([OrderID]) not null
	, [ProductID] int foreign key references [Products]([ProductID]) not null
	, [Quantity] int not null
	, [price] float not null
	, primary key ([OrderDetailID], [ProductID])
)

CREATE TABLE Carts (
    Username nvarchar(30),
    ProductID int,
    Quantity int,
    FOREIGN KEY (Username) REFERENCES Accounts(Username),
    FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
);




